import React, { Component } from 'react';
import '../styles/App.css';

import Address from './Address';
import Collect from './Collect';

import { Tabs, Tab } from 'react-bootstrap';

class App extends Component {
  state = {
    key: 1
  };
  
  titleChange = eventKey => {
    this.setState({
      key: eventKey
    });
  };

  render() {
    const { key } = this.state;

    return (
      <div className="app">
        <div className="app-body">
          <h1>Delivery{key === 1 ? ' address:' : ':'}</h1>
          <Tabs defaultActiveKey={key} id="menu-tabs" onSelect={this.titleChange}>
            <Tab eventKey={1} title="Postal address">
              <Address />
            </Tab>
            <Tab eventKey={2} title="Click & Collect">
              <Collect />
            </Tab>
          </Tabs>
        </div>
      </div>
    );
  }
}

export default App;
