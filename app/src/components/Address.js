import React, { Component } from 'react';
import '../styles/Address.css';

import AddressForm from './AddressForm'

import { Alert, Grid, Row, Col } from 'react-bootstrap'

class Address extends Component {
  state = {
    manualEntry: false,
    postCode: '',
    showError: false
  }

  toggleManualEntry = () => {
    this.setState({
      manualEntry: !this.state.manualEntry
    });
  };

  postCodeEntry = (e) => {
    this.setState({
      postCode: e.target.value
    });
  };

  handleError = () => {
    this.setState({
      showError: !this.state.showError
    });
  };

  validatePostCode = () => {
    if (!this.state.postCode) {
      return this.setState({
        showError: true
      });
    }
    else {
      console.log('working!')
    };
  };

  render() {
    const { showError, manualEntry } = this.state;

    return (
      <div className="address">
        { manualEntry ? 
        <AddressForm toggleManualEntry={this.toggleManualEntry}/>
        :
        <div>
          <h4>Enter postcode</h4>
          <input className="input" onChange={this.postCodeEntry} />
          
          { showError ?  
            <Alert className="address-error" bsStyle="danger" onDismiss={this.handleError}>
              <p><strong>Please enter a valid post code</strong></p>
            </Alert> : '' }

          <div className="address-input-toggle" onClick={this.toggleManualEntry}>
            <p>Enter address manually ></p>
          </div>
          <button onClick={this.validatePostCode} className="address-button">LOOK UP ADDRESS ></button>
        </div>
        }
        <Grid className="address-gift">
          <Row>
            <Col lg={7} md={9} sm={11}>
              <p><strong>is this a gift?</strong></p>
              <p>we'll hide the proces on their order from you</p>
            </Col>
            <Col lg={2} md={3} sm={1}>
              <input className="address-gift" type="checkbox" name="gift" value="gift"/>
            </Col>  
          </Row>
        </Grid>
      </div>
    );
  };
};

export default Address;
