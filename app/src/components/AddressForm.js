import React, { Component } from 'react';
import '../styles/AddressForm.css';

class App extends Component {

  formSubmit = (e) => {
    e.preventDefault()
    console.log('form submitted')
  };

  render() {
    const { toggleManualEntry } = this.props
    return (
      <form className="addressform" onSubmit={this.formSubmit}>
        <div className="addressform-item">
          <p>Name</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Surname</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>House number/flat</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Address line 1</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Address line 2</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Town/City</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>State/Province</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Post/Zip code</p>
          <input type="text" className="input" />
        </div>

        <div className="addressform-item">
          <p>Country</p>
          <input type="text" className="input" />
        </div>

        <button type="submit" className="address-button">Save Address ></button>

        <div className="address-input-toggle" onClick={toggleManualEntry}>
          <p>Enter post code ></p>
        </div>
      </form>
    );
  };
};

export default App;
