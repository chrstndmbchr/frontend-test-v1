import React, { Component } from 'react';
import '../styles/CollectModal.css';

import { Modal, Grid, Row, Col } from 'react-bootstrap'

class CollectModal extends Component {
  state = {
    loading: true,
    postcode: this.props.postcode,
    collections: []
  }

  componentDidMount () {
    console.log('api call')
  }

  render() {
    const { loading } = this.state;

    return (
      <div className="collectmodal">
        <Modal.Header closeButton>
          <Modal.Title>
            Click & Collect
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

          { loading ? 
            <h1 className="collectmodal-loading">Loading</h1>
            :
            <Grid>
              <Row>
                <Col md={7}>
                  <p>test</p>
                </Col>
                <Col md={3}>
                  <p>test</p>
                </Col>
              </Row>
            </Grid>}
            
        </Modal.Body>
      </div>
    );
  };
};

export default CollectModal;
