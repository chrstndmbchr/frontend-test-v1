import React, { Component } from 'react';
import '../styles/Collect.css';

import CollectModal from './CollectModal';

import { FormGroup, InputGroup, FormControl, Alert, Modal } from 'react-bootstrap';

class Collect extends Component {
  state = {
    postcode: '',
    invalidInput: false,
    showModal: false
  }

  handleError = () => {
    this.setState({
      invalidInput: false
    });
  };

  handleShow = () => {
    this.setState({
      showModal: !this.state.showModal
    });
  }

  getPostCode = (e) => {
    this.setState({
      postcode: e.target.value
    });
  } ;

  getCollectionPoints = (e) => {
    e.preventDefault();
    if (!this.state.postcode) {
      return this.setState({
        invalidInput: !this.state.invalidInput
      })
    } else {
      this.setState({
        invalidInput: false,
        showModal: true
      })
    }
  }

  render() {
    const { invalidInput, showModal, postcode } = this.state;

    return (
      <div className="collect">
        <h4>Find your nearest collection point</h4>
        <form onSubmit={this.getCollectionPoints}>
           <FormGroup>
            <InputGroup>
              <FormControl type="text" className="collect-input" onChange={this.getPostCode}/>
              <InputGroup.Addon className="collect-search">
                <button className="collect-button" type="submit">LOOK UP ADDRESS ></button>
              </InputGroup.Addon>
            </InputGroup>
          </FormGroup>
        </form>
        { invalidInput ?  
            <Alert className="collect-error" bsStyle="danger" onDismiss={this.handleError}>
              <p><strong>Please enter a valid post code</strong></p>
            </Alert> : '' }


        <Modal className="test" show={showModal} onHide={this.handleShow} dialogClassName="collectmodal-size">
          <CollectModal 
            postcode={postcode}
            getPostCode={this.getPostCode}
          />
        </Modal>
      </div>
    );
  };
};

export default Collect;
